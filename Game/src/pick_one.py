__author__ = 'Ramon Caraballo'

from src import pokemon

"""

    NOTA:
----------------------------------------------------------------
El elemento o Tipo de pokemon se le debe pasar al momento de crear
una instancia de la clase.

Ejemplo:    charmander = Charmander('F')

Los elementos validos hasta ahora son A, F y H
    Agua, Fuego y Hierva respectivamente.

Hasta el momento se le debe pasar la Inicial tel tipo y en Mayusculas.

----------------------------------------------------------------

"""



class Charmander(pokemon.Pokemon):
    pass


class Bulbasaur(pokemon.Pokemon):
    pass


class Squirtle(pokemon.Pokemon):
    pass