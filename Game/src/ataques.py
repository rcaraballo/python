__author__ = 'Ramon Caraballo'


class Ataques:
    """
    Esta clase contiene todos los ataques disponibles para cada clase o
    tipo de pokemon, ya sea tipo Agua, Fuego o Hierva.
    """

    def obtener_ataques(self, tipo):
        atq = {
            "A": self.agua(),
            "F": self.fuego(),
            "H": self.hierva()
        }

        return atq[tipo]


    def agua(self):

        atq_agua =  [
            {'ataque': {"atk_name": "Placaje", "dano": 5, "usos": 5,
                        "tipo": "N"}},

            {'ataque': {"atk_name": "Burbujas", "dano": 2, "usos": 5,
                        "tipo": "A"}},

            {'ataque': {"atk_name": "Pistola Agua", "dano": 7, "usos": 3,
                        "tipo": "A"}},

            {'ataque': {"atk_name": "Mordisco", "dano": 2, "usos": 5,
                        "tipo": "N"}},
        ]

        return atq_agua

    def fuego(self):
        atq_fuego = [
        {'ataque': {"atk_name": "Araniazo", "dano": 5, "usos": 5,
                    "tipo": 'N'}},

        {'ataque': {"atk_name": "Ascuas", "dano": 6, "usos": 5,
                    "tipo": 'F'}},

        {'ataque': {"atk_name": "Garra Metal", "dano": 7, "usos": 3,
                    "tipo": 'N'}},

        {'ataque': {"atk_name": "Furia", "dano": 2, "usos": 5,
                    "tipo": 'N'}},

        ]

        return atq_fuego

    def hierva(self):
        atq_hierva= [
        {'ataque': {"atk_name": "Placaje", "dano": 5, "usos": 5,
                    "tipo": 'N'}},

        {'ataque': {"atk_name": "Hojas Navajas", "dano": 3, "usos": 5,
                    "tipo": 'H'}},

        {'ataque': {"atk_name": "Doble Filo", "dano": 4, "usos": 3,
                    "tipo": 'N'}},

        {'ataque': {"atk_name": "Rayo Solar", "dano": 7, "usos": 5,
                    "tipo": 'H'}},

        ]

        return atq_hierva
