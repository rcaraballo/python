__author__ = 'Ramon Caraballo'

import random
import ataques


class Pokemon(ataques.Ataques):
    def __init__(self, tipo):

        self.hp = 20
        self.tipo = tipo
        self.devil_a = self.devilidades(self.tipo)
        self.resiste_a = self.resistencia(self.tipo)

        # obtiens los ataques por su tipo
        self.ataques = self.obtener_ataques(self.tipo)


    def info(self):
        return '%s HP: %i' % (self.__class__.__name__, self.hp)

    def barra_hp(self):
        return '=' * self.hp

    def devilidades(self, tipo):
        """
        tipo:A,F,H

        Recive el elemento del Pokemon.(A, F o H)
        Si el pokemon es A: Agua, F: Fuego, H: Hierva.

        Devuelve las devilidades de ese tipo.
        Ejemplo si el Pokemon es tipo A <Agua>
        devuelve su devilidad H <Hierva>
        """
        dev = {
            'F':('A',),
            'A':('H',),
            'H':('F',),
        }# Son tuplas por que un tipo puede tener varias devilidades.

        return dev[tipo]

    def resistencia(self, tipo):
        """
        tipo:A,F,H

        Recive el elemento del Pokemon.(A, F o H)
        Si el pokemon es A: Agua, F: Fuego, H: Hierva.

        Devuelve las resistencia de ese tipo.
        Ejemplo si el Pokemon es tipo A <Agua>
        devuelve a que resiste F <Fuego>
        """
        res = {
            'F':('H',),
            'A':('F',),
            'H':('A',),
        }# Son tuplas por que un tipo puede tener varias elementos a lo que
        #  es resistente.

        return res[tipo]

    def atacar(self, enemigo, atk):
    
        #print '%s a Atacado a %s!!' % (self.__class__.__name__,
        #                              enemigo.__class__.__name__)

        extra = 3 if self.es_critico() else 0

        mensaje = 'Golpe Critico!!' if extra else ''

        return enemigo.recibe(atk['ataque']['dano'] + extra,
                              atk['ataque']['tipo'], mensaje)


    # Devuelven True si son mayores que de lo contrario False.
    lo_esquiva = lambda self: random.randint(1, 10) > 7
    es_critico = lambda self: random.randint(1, 10) > 8


    def recibe(self, dano, atq_tipo, mensaje):
        """
        Se ejecuta cuando un pokemon va a recibir un ataque.
        """

        if self.lo_esquiva():
            print '%s a esquivado el ataque!!' % self.__class__.__name__

        else:
            if atq_tipo in self.devil_a:
                # Si el elemento o tipo de ataque esta entre las devilidades
                # del pokemon le hace 3 mas de dano.
                self.hp -= (dano + 3)

                print mensaje # Presenta el mensaje si es Critico
                print 'Es muy efectivo!!'

            elif atq_tipo in self.resiste_a:
                # Si el elemento o tipo de ataque esta entre las resistencias
                # del pokemon el dano es un punto menos.
                self.hp -= (dano - 1)

                print mensaje # Presenta el mensaje si es Critico
                print 'Es poco efectivo'

            else:
                # De lo contrario el dano del ataque es normal.
                
                print mensaje # Presenta el mensaje si es Critico
                self.hp -= dano
